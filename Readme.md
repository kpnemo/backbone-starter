# Backbone Starter
Clea template to start your backbone based client-side app.

## Install
Please, open index.html in your browser to see demo.

## Includes
	* jQUery
	* Underscore.js
	* Backbone.js
	* Twitter Bootstrap

