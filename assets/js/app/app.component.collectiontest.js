/**
 * Component Example
 * Url: example or empty
 * @type {*}
 */
var APP = (function(SCOPE){

	//Define variables on top
	var Model,
		View,
		Collection,
		Component = {};

	/**
	 * Component model
	 * @type {*}
	 */
	Component.Model = Backbone.Model.extend({
		defaults: {
			name: '',
			lastName: '',
			phone: ''
		},
		initialize: function(){
			//This is a contructor
			//You can put some code here to run on init model
		},
		parse: function(respond){
			//You can customize the parsing results here
			return respond;
		}
	});

	/**
	 * Collection
	 * @type {*}
	 */
	Component.Collection = Backbone.Collection.extend({
		url: 'test_json/exampleCollection.json',
		model: Component.Model,
		initialize: function(){
			//Run code in collection constructor
			_.bindAll(this, 'parse');
		},
		parse: function(respond){
			//You can customize the parsing results here
			return respond;
		}
	});

	/**
	 * ModelDetailView
	 * @type {*}
	 */
	Component.ModelDetailView = Backbone.View.extend({
		tagName: 'div',
		className: 'modal hide fade model-detail-view',
		initialize: function(){
			_.bindAll();
		},
		events: {
			'click .btn': function(ev){
				ev.preventDefault();
				$(this.el).modal('toggle');
				$(this.el).remove();
			}
		},
		render: function(){
			var _template = _.template($('#template_modeltest_item_modal').html());
			$(this.el).html(_template(this.model.toJSON()));

			return this;
		}
	});

	/**
	 *  ModelView
	 * generating html, using a model as a data
	 * @type {*}
	 */
	Component.ModelView = Backbone.View.extend({
		tagName: 'tr',
		className: 'vcard-item',
		initialize: function(){
			var that = this;
			_.bindAll(this, 'render', 'showDetails');
		},
		events: {
			'click': 'showDetails'
		},
		showDetails: function(ev){
			var that = this,
				_modalTemplate = new Component.ModelDetailView({model: this.model});

			_modalTemplate.render();
			$('#main').append(_modalTemplate.$el);
			$('.model-detail-view').modal();
		},
		render: function(){
			var _template = _.template($('#template_modeltest_item').html());
			$(this.el).html( _template(this.model.toJSON())); //set html to this.el

			return this; //don't forget to return self
		}
	});

	/**
	 * CollectionView
	 * generating html, using a model as a data
	 * @type {*}
	 */
	Component.CollectionView = Backbone.View.extend({
		tagName: 'div',
		className: 'collection-test-view',
		initialize: function(){
			var that = this;
			_.bindAll(this, 'render');

			//Listen on change event to know when data ready
			this.collection.on('reset', function(){
				that.trigger('dataReady');
				that.render(); //render view on change
			});

			this.collection.fetch(); //fetch the data
		},
		render: function(){
			var _template = _.template($('#template_collectiontest').html()),
				that = this;
			$(this.el).html( _template({size: this.collection.models.length})); //set html to this.el

			_.each(this.collection.models, function(model){
				var _modelView = new Component.ModelView({model: model});
				_modelView.render();
				$(that.el).find('.put_models_here').append($(_modelView.$el));
			});

			this.trigger('renderDone'); //fire renderDone
			return this; //don't forget to return self
		}
	});


	/**
	 * Bind your routes in this function
	 * @param controller
	 */
	bindRoutes = function(controller){

		//subscribe to route:modelTest event from controller
		controller.on('route:collectionTest', function(){
			Collection = new Component.Collection(); //init model
			View = new Component.CollectionView({collection: Collection}); //init view and pass model

			//Subscribe to renderDone
			View.on('renderDone', function(){
				$('#main').html(View.$el); //put the view into DOM
			});
		});

		//On link #modeltest fire an event route:modelTest
		controller.route('collectiontest', 'collectionTest');
	};


	//Listen to routerReady and go bing your routes
	SCOPE.Events.on('routerReady', bindRoutes);

	return SCOPE;

})(APP || {})