<script type="text/template" id="template_emptyDefault">
	<div class="row">
		<div class="span12">
			<h3>Aloha username,</h3>
			<p>This is an empty template. You can find me in app.templates.tpl</p>
			<p>Click on <a href="#example">Example</a> to switch to Example Component.</p>
		</div>
	</div>
</script>

<script type="text/template" id="template_example">
	<div class="row">
		<div class="span12">
			<h3>Huston, Huston, this is example…</h3>
			<p>
				You can bind an events. Lets try to bind this button: <a class="btn btn-primary" id="bindTest">Click</a>
			</p>
		</div>
		<div class="span12 hide example-menu">
			<ul class="nav nav-pills">
				<li class=""><a href="#example/1">Example 1</a></li>
				<li class=""><a href="#example/2">Example 2</a></li>
			</ul>
		</div>
	</div>
</script>

<script type="text/template" id="template_modeltest">
	<div class="row">
		<div class="span12">
			<div class="well well-large">
				<h1>Vcard</h1>
				<p>
					The table bellow populated from a model
				</p>
			</div>
			<table class="table table-hover table-bordered" id="vcard">
				<thead>
					<tr><td><strong>#Name</strong></td><td><strong>#Last Name</strong></td><td><strong>#Phone</strong></td></tr>
				</thead>
				<tbody>
					<tr><td><%=name%></td><td><%=lastName%></td><td><%=phone%></td></tr>
				</tbody>
			</table>
		</div>
	</div>
</script>

<script type="text/template" id="template_collectiontest">
	<div class="row">
		<div class="span12">
			<div class="well well-large">
				<h1>Vcards</h1>
				<p>
					See the table, its a collection of <strong><%=size%></strong> models,
					and every model rendered by model template
				</p>
				<h3>Click on the rows, to see more info.</h3>
			</div>
			<table class="table table-hover table-bordered" id="vcards">
				<thead>
				<tr><td><strong>#Name</strong></td><td><strong>#Last Name</strong></td><td><strong>#Phone</strong></td></tr>
				</thead>
				<tbody class="put_models_here"></tbody>
			</table>
		</div>
	</div>
</script>

<script type="text/template" id="template_modeltest_item">
	<td><%= name %></td>
	<td><%= lastName %></td>
	<td><%= phone %></td>
</script>

<script type="text/template" id="template_modeltest_item_modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><%=name%> info</h3>
	</div>
	<div class="modal-body">
		<dl>
			<dt></dt>
			<dd><strong>Last Name</strong>: <%= lastName %></dd>
			<dd><strong>Phone</strong>: <%= phone %></dd>
		</dl>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn">Close</a>
	</div>
</script>