/**
 * CONTROLLER
 * Events: routerInit, routerReady
 * @type {*}
 */

var APP = (function(SCOPE){

	var Controller = Backbone.Router.extend({
		initialize: function(){
			SCOPE.Events.trigger('routerInit', SCOPE.Controller);
		},

		/**
		 * Leave the routes  empty here
		 * its better to listen to routerReady event,
		 * and add routes from your components
		 * this way you can remove or add components with no additional Router modification
		 */
		routes: {}
	});

	SCOPE.Events.on('systemStart', function(){
		SCOPE.Controller = new Controller();
		SCOPE.Events.trigger('routerReady', SCOPE.Controller);
	});

	return SCOPE;
})(APP || {});