/**
 * Component Default
 * Url: empty
 * @type {*}
 */
var APP = (function(SCOPE){

	var Model,
		View,
		Component = {};

	Component.Model = Backbone.Model.extend({});

	Component.View = Backbone.View.extend({
		tagName: 'div',
		className: 'default-view',
		initialize: function(){
		},
		render: function(){
			var _template =  _.template($('#template_emptyDefault').html());
			$(this.el).html(_template);

			return this;
		},
		events: {}
	});

	bindRoutes = function(controller){

		controller.on('route:emptyDefault', function(){
			View = new Component.View();
			View.render();
			$('#main').html(View.$el);
		});

		controller.route('', 'emptyDefault');
	};


	SCOPE.Events.on('routerReady', bindRoutes);

	return SCOPE;

})(APP || {})