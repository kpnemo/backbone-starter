var APP = (function(SCOPE){

	/**
	 * Helper function to retrive the tempalte
	 * You can include the templates file into your body with php
	 * or other backend language
	 * @param callback
	 * @param url
	 * @private
	 */
	var _loadTemplate = function(callback, url){
		var templateUrl = url || 'assets/js/app/app.templates.tpl',
			callbackFn = callback || function(){};

		$.ajax({
			url: 'assets/js/app/app.templates.tpl',
			type: 'GET',
			success: function(respond){
				$('body').append(respond);
			},
			complete: function(){
				SCOPE.Events.trigger('templatesReady');
				callbackFn.call();
			}
		})
	}
	_.bind(_loadTemplate, SCOPE);


	SCOPE.Events.on('winLoad', function(){
		_loadTemplate(function(){
			SCOPE.Events.trigger('systemStart');
		});
	});

	/**
	 * On routerReady start the the history (see Backbone documentation)
	 * Events: historyStarted
	 */
	SCOPE.Events.on('routerReady', function(){
		Backbone.history.start();
		SCOPE.Events.trigger('historyStarted', SCOPE.Controller);
	});

	SCOPE.Events.on('domReady', function(){
		//Put your action here for domReady
	});


	//remap events to general Events object
	$(document).on('ready', function(){
		SCOPE.Events.trigger('domReady');
	});

	$(window).on('load', function(){
		SCOPE.Events.trigger('winLoad');
	});

	return SCOPE;
})(APP || {});