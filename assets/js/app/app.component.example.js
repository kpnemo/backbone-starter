/**
 * Component Example
 * Url: example or empty
 * @type {*}
 */
var APP = (function(SCOPE){

	var Model,
		View,
		Component = {};

	Component.Model = Backbone.Model.extend({

	});

	Component.View = Backbone.View.extend({
		tagName: 'div',
		className: 'example-page',
		initialize: function(){
			_.bindAll(this, 'showSubMenu');
		},
		render: function(){
			var _template = _.template($('#template_example').html());
			$(this.el).html(_template());

			return this;
		},
		events: {
			'click #bindTest': 'showSubMenu'
		},
		showSubMenu: function(ev){
			ev.preventDefault();
			if($(this.el).find('.example-menu').hasClass('hide')){
				$(this.el).find('.example-menu').removeClass('hide');
			} else {
				$(this.el).find('.example-menu').addClass('hide');
			}
		}
	});

	bindRoutes = function(controller){

		controller.on('route:exampleGeneral', function(a,b,c){
			View = new Component.View();
			View.render();
			$('#main').html(View.$el);
		});
		controller.on('route:exampleShowFirst', function(){
			console.log('route example/1');
		});
		controller.on('route:exampleShowSecond', function(){
			console.log('route example/2');
		});

		controller.route('example', 'exampleGeneral');
		controller.route('example/1', 'exampleShowFirst');
		controller.route('example/2', 'exampleShowSecond');
	};


	SCOPE.Events.on('routerReady', bindRoutes);

	return SCOPE;

})(APP || {})