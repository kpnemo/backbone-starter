/**
 * Component Example
 * Url: example or empty
 * @type {*}
 */
var APP = (function(SCOPE){

	//Define variables on top
	var Model,
		View,
		Component = {};

	/**
	 * Component model
	 * @type {*}
	 */
	Component.Model = Backbone.Model.extend({
		defaults: {
			name: '',
			lastName: '',
			phone: ''
		},
		url: 'test_json/exampleModel.json',
		initialize: function(){
			//This is a contructor
			//You can put some code here to run on init model
		},
		parse: function(respond){
			//if you want to parse the server response
			this.set(respond);
		}
	});


	/**
	 * View
	 * generating html, using a model as a data
	 * @type {*}
	 */
	Component.View = Backbone.View.extend({
		tagName: 'div',
		className: 'model-test-view',
		initialize: function(){
			var that = this;
			_.bindAll(this, 'render');

			//Listen on change event to know when data ready
			this.model.on('change', function(){
				that.trigger('dataReady');
				that.render(); //render view on change
			});

			this.model.fetch(); //fetch the data
		},
		render: function(){
			var _template = _.template($('#template_modeltest').html());
			$(this.el).html( _template(this.model.toJSON()) ); //set html to this.el

			this.trigger('renderDone'); //fire renderDone
			return this; //don't forget to return self
		}
	});

	/**
	 * Bind your routes in this function
	 * @param controller
	 */
	bindRoutes = function(controller){

		//subscribe to route:modelTest event from controller
		controller.on('route:modelTest', function(){
			Model = new Component.Model(); //init model
			View = new Component.View({model: Model}); //init view and pass model

			//Subscribe to renderDone
			View.on('renderDone', function(){
				$('#main').html(View.$el); //put the view into DOM
			});
		});

		//On link #modeltest fire an event route:modelTest
		controller.route('modeltest', 'modelTest');
	};


	//Listen to routerReady and go bing your routes
	SCOPE.Events.on('routerReady', bindRoutes);

	return SCOPE;

})(APP || {})