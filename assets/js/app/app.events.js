/**
 * EVENTS
 * Generic events object that supports
 * Methods: on, off, trigger
 * @type {*}
 */

var APP = (function(SCOPE){

	SCOPE.Events = function(){};
	_.extend(SCOPE.Events, Backbone.Events);

	return SCOPE;
})(APP || {});